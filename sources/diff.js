var moment = require('moment');

module.exports = function isBetween(){
    var start = '2010-10-25';
    var end = '2010-12-25';

    // The first argument can be a MomentObject | String | Number | Date | Array
    // by default return the time in milliseconds
    var time = moment(end).diff(start);
    console.log('time: ', time);

    // The second argument define the unit of measurement
    // milliseconds | seconds | minutes | hours | ...
    var timeInDays = moment(end).diff(start, 'days');
    console.log('timeInDays: ', timeInDays);

    // You can get a floating point number with the third argument
    var timeInWeeksWithFloats = moment(end).diff(start, 'weeks', true);
    console.log('timeInWeeksWithFloats: ', timeInWeeksWithFloats);

    return timeInDays;
};