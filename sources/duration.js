var moment = require('moment');

module.exports = function duration(){
    var start = '2010-10-25';
    var end = '2010-12-25';

    // Get the difference between two dates (see diff.js)
    var time = moment(end).diff(start);
    console.log('time: ', time);

    // The first argument of duration method can be a
    // Number | Object | String
    // by default return a Moment Object
    var durationObject = moment.duration(time, 'seconds');

    // humanize method has an argument to add suffix
    var humanizedDurationWithoutSuffix = durationObject.humanize();
    console.log('humanizedDurationWithoutSuffix: ', humanizedDurationWithoutSuffix);

    var humanizedDurationWithSuffix = durationObject.humanize(true);
    console.log('humanizedDurationWithSuffix: ', humanizedDurationWithSuffix);

    // weeks method return the weeks about the dates (0 - 4)
    var durationInWeeks = durationObject.weeks();
    console.log('durationWeeks: ', durationInWeeks);

    // asWeeks method return the length of the duration in weeks
    var durationInWeeksLength = durationObject.asWeeks();
    console.log('durationInWeeksLength: ', durationInWeeksLength);


    return humanizedDurationWithoutSuffix;
};