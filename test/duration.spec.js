var expect = require('expect.js');
var moment = require('moment');
var duration = require('../sources/duration.js');

suite('Setting a moment at the end of an interval', function(){

    test('Should return humanized duration in years without suffix', function(){
        var start = '2010-10-25';
        var end = '2010-12-25';
        var time = moment(end).diff(start);
        var durationObject = moment.duration(time, 'seconds');
        var expected = durationObject.humanize();
        var result = duration();
        expect(result).to.be(expected);
    });

});