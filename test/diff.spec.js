var expect = require('expect.js');
var moment = require('moment');
var diff = require('../sources/diff.js');

suite('Querying differences between moments', function(){

    test('Should return a time in milliseconds', function(){
        var start = '2010-10-25';
        var end = '2010-12-25';
        var expected = moment(end).diff(start, 'days');
        var result = diff();
        expect(result).to.be(expected);
    });

});