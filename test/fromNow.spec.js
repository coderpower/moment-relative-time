var expect = require('expect.js');
var moment = require('moment');
var fromNow = require('../sources/fromNow.js');

suite('Setting a moment at the end of an interval', function(){

    test('Should return a date in string format', function(){
        var date = '2010-10-25';
        var expected = moment(date).fromNow();
        var result = fromNow();
        expect(result).to.be(expected);
    });

});