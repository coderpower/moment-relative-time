### Relative Time

Showcasing fromNow

```javascript
    var date = '2010-10-25';
    var howLongAgoWithSuffix = moment(date).fromNow();
    var howLongAgoWithoutSuffix = moment(date).fromNow(true);
```
For more information, please reer to the documentation: http://momentjs.com/docs/#/displaying/fromnow/

Creating a diff between moments

```javascript
    var start = '2010-10-25';
    var end = '2010-12-25';
    var time = moment(end).diff(start);
    var timeInDays = moment(end).diff(start, 'days');
    var timeInWeeksWithFloats = moment(end).diff(start, 'weeks', true);

```
For more information, please reer to the documentation: http://momentjs.com/docs/#/displaying/difference/

Can create and humanize durations

```javascript
    var start = '2010-10-25';
    var end = '2010-12-25';
    var time = moment(end).diff(start);
    var durationObject = moment.duration(time, 'seconds');
    var humanizedDurationWithoutSuffix = durationObject.humanize();
    var humanizedDurationWithSuffix = durationObject.humanize(true);
    var durationInWeeks = durationObject.weeks();
    var durationInWeeksLength = durationObject.asWeeks();
```

For more information, please reer to the documentation: http://momentjs.com/docs/#/durations/


